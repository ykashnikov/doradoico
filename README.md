# DoradoICO

Important: restart ganache-cli after each test (rounds.js and doradotoken.js) because of the time shift

Token contract with integrated ICO for Dorado.

## BUILD & TEST
```
npm install
npm run-script ganache-cli
truffle test # or npm run test
``` 

## ICO Periods
| N | Sale name |  From                  |      Till             | Token Cap  | Bonus |
|---|-----------|------------------------|-----------------------|------------|-------|
| 1 | HOT sale  |  2018.02.07 1517961600 | 2018.02.21 1519171200 | 70,000,000 | 33    |
| 2 | Sale A    |  2018.02.21 1519171200 | 2018.03.07 1520380800 | 70,000,000 | 30    |
| 3 | Sale B    |  2018.03.07 1520380800 | 2018.03.21 1521590400 | 70,000,000 | 27    |
| 4 | Sale C    |  2018.03.21 1521590400 | 2018.04.04 1522800000 | 75,000,000 | 22    |
| 5 | Sale D    |  2018.04.04 1522800000 | 2018.04.18 1524009600 | 75,000,000 | 17    |
| 6 | Sale E    |  2018.04.18 1524009600 | 2018.05.02 1525219200 | 75,000,000 | 12    |
| 7 | Sale F    |  2018.05.02 1525219200 | 2018.05.16 1526428800 | 75,000,000 |  7    |

Sale round based on the conjunction of time and capacity: Whatever is reached first starts the next round. Unsold tokens get passed to the next round.

## Token distribution

| who           | percent |
|---------------|---------|
| Crowd         | 51      |
| Team          | 15      |
| Foodout group |  8      |
| Early investors | 8     |
| Advisors        | 7     |
| Legal partners  | 5     |
| Other partner   | 4     |
| Bounty          | 2     |

Team and foodout group tokens are locked for 3 years.
Other reserved tokens are available immediately after the ICO was closed. 

Note: Reserved tokens are given in percent to the planned hard cap of 1,000,000,000 DOR. They are absolute and do not dependent on the actual amount of issued tokens.

## Token and ICO parameters

Decimals: 15

Symbol: DOR

Base Price: 1 Ether = 6667 DOR

Min goal: 23,000,000 DOR

Planned hard cap: 1,000,000,000 DOR


## ICO special conditions

- The owner is able to issue tokens in arbitrary height until he officially closes the ICO. This was asked for, because they're going to accept other currencies as well and want to make sure that they can issue tokens for everybody. Planned hardcap including reserved tokens: 1,000,000,000 DOR, but no enforcement.
- Tokend not transferable until the ICO was officially closed by the owner.